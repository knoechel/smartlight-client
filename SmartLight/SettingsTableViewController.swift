//
//  SettingsTableViewController.swift
//  SmartLight
//
//  Created by Hans Knöchel on 27.01.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {

    @IBAction func logoutAction(sender: AnyObject) {
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "loggedIn")
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

}
