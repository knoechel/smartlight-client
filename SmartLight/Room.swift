//
//  Room.swift
//  SmartLight
//
//  Created by Hans Knöchel on 28.01.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import UIKit
import SwiftyJSON

public class Room: NSObject {
    private var name : String = ""
    private var id : String = ""
    private var lights = [JSON]()
    
    public func getLights() -> [JSON] {
        return self.lights
    }
    
    public func getLightsString() -> String {
        let suffix = self.lights.count == 1 ? "Licht" : "Lichter"
        return String(self.lights.count) + " " + suffix
    }
    
    public func getName() -> String {
        return self.name
    }
    
    public func getId() -> String {
        return self.id
    }
    
    public func setName(name : String) -> Void {
        self.name = name
    }
    
    public func setLights(lights : [JSON]) -> Void {
        self.lights = lights
    }
    
    public func setId(id : String) {
        self.id = id
    }
}
