//
//  LoginTableViewController.swift
//  SmartLight
//
//  Created by Hans Knöchel on 03.01.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD
import Foundation

class LoginTableViewController: UITableViewController {

    /// The username field
    @IBOutlet weak var usernameField: UITextField!

    /// The password field
    @IBOutlet weak var passwordField: UITextField!
    
    /// Select the password field when pressing the return button
    ///
    /// :param: sender The sender of the event
    @IBAction func selectPasswordField(sender: UITextField) {
        self.passwordField.becomeFirstResponder()
    }
    
    /// Blur the password field when pressing the return button
    ///
    /// :param: sender The sender of the event
    @IBAction func passwordReturnKeyAction(sender: UITextField) {
        self.passwordField.resignFirstResponder()
    }
    
    /// Open the register view
    @IBAction func openRegisterView() {
        let storyboard = UIStoryboard(name: "Register", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("registerView") as! UINavigationController

        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    /// Blur the password field and submit the login data
    ///
    /// :param: sender The sender of the event
    @IBAction func submitAction(sender: AnyObject) {
        self.passwordField.resignFirstResponder()
        self.submitLoginData()
    }
    
    /// Submit the login data
    func submitLoginData() {
        let usernameValue = self.usernameField.text
        let passwordValue = self.passwordField.text
        let url : String = NSUserDefaults.standardUserDefaults().stringForKey("APIURL")!
        
        let loader = Loader(view: self.view, title: "Logging In ...")
        loader.show()

        let request = Alamofire.request(.POST, url+"/authenticate", parameters: ["username": usernameValue, "password": passwordValue], encoding: .JSON)
            .responseJSON { (req, res, json, error) in

                loader.hide()

                if(json == nil) {
                    BarNotification(title: "The server is currently unavailable!").show()
                    return;
                }
                
                let responseData = JSON(json!)
                
                if(res?.statusCode != 200) {
                    Utilities().showAlert("Login error", message: responseData["message"].stringValue, view: self)
                } else {
                    NSUserDefaults.standardUserDefaults().setBool(true, forKey: "loggedIn")
                    NSLog("Success: \(responseData)")
                    self.openHomeView()
                }
        }
        println(request)
    }
    
    /// Open the home view
    func openHomeView() -> Void {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("HomeView") as! UITabBarController
        
        self.dismissViewControllerAnimated(false, completion: nil)
        self.presentViewController(vc, animated: false, completion: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        
        let currentDevice : String = UIDevice().model
        
        // Form prefilling in Simulator
        if(currentDevice.rangeOfString("Simulator")?.isEmpty === false) {
            self.usernameField.text = "hans"
            self.passwordField.text = "test"
        }
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
}
