//
//  RoomsTableViewController.swift
//  SmartLight
//
//  Created by Hans Knöchel on 27.01.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RoomsTableViewController: UITableViewController {
    
    // Store all rooms
    var items : NSMutableArray = []
    
    // Store the raw rooms-json-response
    var rooms : JSON = nil
    
    // Initialize the beaconListener for backgroundServices
    let beaconListener = BeaconListener()
    
    /// Force refresh
    ///
    /// :param: sender The sender of the event
    @IBAction func pulledRefreshAction(sender: AnyObject) {
       self.loadRooms()
    }
    
    /// Load the rooms
    internal func loadRooms() {
        let url : String = NSUserDefaults.standardUserDefaults().stringForKey("APIURL")!
        
        let loader = Loader(view: self.navigationController!.view, title: "Loading rooms ...")
        loader.show()
        
        let request = Alamofire.request(.GET, url+"/rooms", encoding: .JSON)
            .responseJSON { (req, res, json, error) in
                
                loader.hide()
               
                if(json == nil) {
                    self.refreshControl?.endRefreshing()
                    BarNotification(title: "The server is currently not available!").show()
                    return;
                }
                
                let responseData = JSON(json!)
                
                if(res?.statusCode != 200) {
                    BarNotification(title: responseData["message"].stringValue).show()
                } else {
                 //   NSLog("Success: \(responseData)")
                    self.rooms = responseData
                    self.displayRooms()
                }
        }
        println(request)
    }
    
    /// Display the rooms
    func displayRooms() -> Void {
        self.items.removeAllObjects()
        
        for (key: String, subJson: JSON) in self.rooms {
            let index : Int = key.toInt()!
            
            let id = self.rooms[index]["_id"].stringValue
            let name = self.rooms[index]["name"].stringValue
            let lights = self.rooms[index]["lights"].arrayValue
            
            var room = Room()
            room.setId(id)
            room.setName(name)
            room.setLights(lights)
            
            self.items.addObject(room)
        }
        
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
    /// Delete a specific room
    ///
    /// :param: singleRoom The room to be deleted
    func deleteRoom(singleRoom : Room) {
        let url : String = NSUserDefaults.standardUserDefaults().stringForKey("APIURL")!

        let loader = Loader(view: self.navigationController!.view, title: "Removing room ...")
        loader.show()

        let request = Alamofire.request(.DELETE, url+"/rooms/"+singleRoom.getId(), encoding: .JSON)
            .responseJSON { (req, res, json, error) in
                
                loader.hide()
                
                if(res?.statusCode != 200) {
                    BarNotification(title: "Invalid room given").show()
                } else {
                    Utilities().showAlert("Room removed", message: "The room was successfully removed!", view: self.parentViewController!)
                    
                    self.items.removeObject(singleRoom)
                    self.tableView.reloadData()
                }
        }
        println(request)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.beaconListener.registerRegion()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.loadRooms()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "DetailSegue") {
            var svc = segue.destinationViewController as! RoomDetailsTableViewController;
            var selectedIndexPath : NSIndexPath = self.tableView.indexPathForSelectedRow()!
            
            svc.roomDetails = self.items.objectAtIndex(selectedIndexPath.row) as! Room
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        self.deleteRoom((self.items.objectAtIndex(indexPath.row) as! Room))
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("RoomCell", forIndexPath: indexPath) as! UITableViewCell
        let cellData = self.items.objectAtIndex(indexPath.row) as! Room

        cell.textLabel?.text = cellData.getName()
        cell.detailTextLabel?.text = cellData.getLightsString()

        return cell
    }
}
