//
//  HomeTabBarController.swift
//  SmartLight
//
//  Created by Hans Knöchel on 05.02.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import UIKit

class HomeTabBarController: UITabBarController {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}