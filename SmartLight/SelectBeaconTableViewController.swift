//
//  SelectBeaconTableViewController.swift
//  SmartLight
//
//  Created by Hans Knöchel on 11.02.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class SelectBeaconTableViewController: UITableViewController, BeaconManagerDelegate {
    
    /// The beacons to be displayed
    private var items : NSMutableArray = []
    
    /// The selected beacon models
    internal var selectedBeacons = [Beacon]()
    
    /// The beacon manager to fire location updates
    private let beaconManager = BeaconManager()

    /// The submit button
    @IBOutlet weak var submitButton: UIBarButtonItem!

    /// Force refresh
    ///
    /// :param: sender The sender of the event
    @IBAction func pulledRefreshAction(sender: UIRefreshControl) {
        self.beaconManager.startRanging()
        self.refreshControl?.endRefreshing()
    }
    
    /// Cancel the beacon selection
    ///
    /// :param: sender The sender of the event
    @IBAction func cancelAction(sender: UIBarButtonItem) {
        self.closeWindow()
    }
    
    /// Enable or disable the submit button depending on the selection
    func toggleSubmitButton() {
        self.submitButton.enabled = (self.selectedBeacons.count > 0)
    }
    
    /// Close the current window
    func closeWindow() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    /// Fired by the delegate if bluetooth or location services are not enabled
    func didReceiveRangingError(error: NSError) {
        Utilities().showAlert("Location not available", message: "The location services are not available. Please make sure that you have granted the permissions to use your location & bluetooth and try again.", view: self)
    }
    
    /// Fired by the delegate when receiving new beacons
    func didRangeBeacons(beacons : [AnyObject]!) {
        self.items.removeAllObjects()
        
        for beacon in beacons {
            let beaconObject = Beacon(beacon: beacon as! CLBeacon)
            self.items.addObject(beaconObject)
        }
        
        self.beaconManager.stopRanging()
        self.tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.beaconManager.delegate = self
        self.beaconManager.startRanging()
    }
    
    override func viewWillAppear(animated: Bool) {
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("BeaconCell", forIndexPath: indexPath)as! UITableViewCell
        let myItem = self.items.objectAtIndex(indexPath.row) as! Beacon
        
        cell.textLabel?.text = myItem.getBeacon().proximityUUID.UUIDString
        cell.accessoryType = (myItem.selected) ? .Checkmark : .None
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) -> Void {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let tappedItem = self.items.objectAtIndex(indexPath.row) as! Beacon
        tappedItem.selected = !tappedItem.selected
        
        if(contains(self.selectedBeacons, tappedItem)) {
            self.selectedBeacons.removeAtIndex(find(self.selectedBeacons, tappedItem)!)
        } else {
            self.selectedBeacons.append(tappedItem)
        }
        
        self.toggleSubmitButton()
        
        tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
    }
}
