//
//  RegisterTableViewController.swift
//  SmartLight
//
//  Created by Hans Knöchel on 26.01.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD

class RegisterTableViewController: UITableViewController {

    
    /// The username field
    @IBOutlet weak var usernameField: UITextField!
    
    /// The password field
    @IBOutlet weak var passwordField: UITextField!

    /// Submit the registration data
    @IBAction func submitRegistration() {
        let usernameValue = self.usernameField.text
        let passwordValue = self.passwordField.text
        
        let url : String = NSUserDefaults.standardUserDefaults().stringForKey("APIURL")!
        
        let loader = Loader(view: self.view, title: "Registering ...")
        loader.show()
        
        let request = Alamofire.request(.POST, url+"/users", parameters: ["username": usernameValue, "password": passwordValue], encoding: .JSON)
            .responseJSON { (req, res, json, error) in
    
                if(json == nil) {
                    BarNotification(title: "The server is currently not available!").show()
                    return;
                }

                loader.hide()
                let responseData = JSON(json!)

                if(res?.statusCode != 200) {
                    Utilities().showAlert("Registration error", message: responseData["message"].stringValue, view: self)
                } else {
                    NSLog("Success: \(responseData)")
                    Utilities().showAlert("Registration", message: "Registration successfully completed!", view: self)
                }
        }
        
        println(request)
    }
    
    /// Close the registration view
    @IBAction func closeRegisterView(sender: UIBarButtonItem) {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
}
