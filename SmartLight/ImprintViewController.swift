//
//  ImprintViewController.swift
//  SmartLight
//
//  Created by Hans Knöchel on 23.04.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import UIKit

class ImprintViewController: UIViewController {
   
    @IBOutlet weak var webView: UIWebView!
    
    override  func viewWillAppear(animated: Bool) {
        let url = NSBundle.mainBundle().URLForResource("Imprint", withExtension:"html")

        let myRequest = NSURLRequest(URL: url!);
        self.webView.loadRequest(myRequest);
    }
}
