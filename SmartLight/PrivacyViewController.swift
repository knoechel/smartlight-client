//
//  PrivacyViewController.swift
//  SmartLight
//
//  Created by Hans Knöchel on 23.04.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import UIKit

class PrivacyViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    
    override  func viewWillAppear(animated: Bool) {
        let url = NSBundle.mainBundle().URLForResource("Privacy", withExtension:"html")
        
        let myRequest = NSURLRequest(URL: url!);
        self.webView.loadRequest(myRequest)        
    }
}
