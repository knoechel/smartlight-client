//
//  RoomDetailsTableViewController.swift
//  SmartLight
//
//  Created by Hans Knöchel on 27.01.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RoomDetailsTableViewController: UITableViewController, UIActionSheetDelegate {

    /// The lights to be displayed
    var items : NSMutableArray = []
    
    // The raw lights-json-response
    var lights : JSON = nil

    // The given room object from the room list
    var roomDetails : Room!
    
    /// Show an action dialog to confirm the destructive oepration
    ///
    /// :param: sender The sender of the event
    @IBAction func prepareDeleteAction(sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Remove room", message: "Do you really want to remove this room? This step can not be revoked!", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
        }
        alertController.addAction(cancelAction)
        
        let destroyAction = UIAlertAction(title: "Remove", style: .Destructive) { (action) in
            self.deleteAction()
        }
        alertController.addAction(destroyAction)
        
        self.presentViewController(alertController, animated: true) {
            // ...
        }
    }
    
    /// Delete the current room
    func deleteAction() {
        let url : String = NSUserDefaults.standardUserDefaults().stringForKey("APIURL")!
        
        let loader = Loader(view: self.tableView, title: "Removing room ...")
        loader.show()
        
        let request = Alamofire.request(.DELETE, url+"/rooms/"+self.roomDetails.getId(), encoding: .JSON)
            .responseJSON { (req, res, json, error) in
                
                loader.hide()
                
                if(res?.statusCode != 200) {
                    BarNotification(title: "Invalid room given").show()
                } else {
                    self.navigationController?.popViewControllerAnimated(true)
                }
        }
        println(request)
        
    }
    
    /// Display the lights of the room
    func displayLights() -> Void {
        self.items.removeAllObjects()
        
        println(self.lights)
        for (key: String, subJson: JSON) in self.lights {
            let index : Int = key.toInt()!
            
            let id = self.lights[index]["id"].intValue
            let name = self.lights[index]["name"].stringValue
            let isOnline = self.lights[index]["state"]["reachable"].boolValue
            
            var light = Light()
            light.setId(id)
            light.setName(name)
            light.isOnline = isOnline
            
            self.items.addObject(light)
        }
        
        self.tableView.reloadData()
    }
    
    /// Load the room details for further informations
    func loadDetails() {
        let url : String = NSUserDefaults.standardUserDefaults().stringForKey("APIURL")!
        
        let loader = Loader(view: self.navigationController!.view, title: "Loading details ...")
        loader.show()
        
        let request = Alamofire.request(.GET, url+"/rooms/"+self.roomDetails.getId(), encoding: .JSON)
            .responseJSON { (req, res, json, error) in
                
                loader.hide()
                
                if(json == nil) {
                    self.refreshControl?.endRefreshing()
                    BarNotification(title: "The server is currently not available!").show()
                    return;
                }
                
                let responseData = JSON(json!)
                
                if(res?.statusCode != 200) {
                    BarNotification(title: responseData["message"].stringValue).show()
                } else {
                    self.lights = responseData["lights"]
                    self.displayLights()
                }
        }
        println(request)
    }
    
    override func viewWillAppear(animated: Bool) {
        self.loadDetails()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let indexPath = tableView.indexPathForSelectedRow();
       // self.roomDetails.getId()
        
        let actionSheet = UIActionSheet(title: "Would you like to toggle the light?", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Toggle off", "Toggle on")
        actionSheet.actionSheetStyle = .Default
        actionSheet.showInView(self.view)
        
        self.tableView.deselectRowAtIndexPath(indexPath!, animated: true)
    }
    
    // MARK: UIActionSheetDelegate
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        switch buttonIndex {
        case 1: self.toggleLightState(0)
        break;
        case 2: self.toggleLightState(1)
        break;
        default: break;
        }
    }
 
    func toggleLightState(state : Int) {
        let url : String = NSUserDefaults.standardUserDefaults().stringForKey("APIURL")!
        let request = Alamofire.request(.POST, url+"/rooms/"+self.roomDetails.getId()+"/state", parameters: ["lightState": state],encoding: .JSON)
            .responseJSON { (req, res, json, error) in
                if(json == nil) {
                    return;
                }
                
                let responseData = JSON(json!)
                println(responseData)
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("LightCell", forIndexPath: indexPath) as! UITableViewCell
        let myItem = self.items.objectAtIndex(indexPath.row) as! Light
        
        var lightStatus = cell.viewWithTag(100)
        var lightName = cell.viewWithTag(101) as! UILabel
        
        lightStatus?.backgroundColor = (myItem.isOnline) ? UIColor(rgba: "#17810f") : UIColor(rgba: "#ccc")
        lightName.text = myItem.getName()
        
        return cell
    }
}
