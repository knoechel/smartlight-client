//
//  BeaconManager.swift
//  SmartLight
//
//  Created by Hans Knöchel on 05.02.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON

protocol BeaconManagerDelegate {
    func didRangeBeacons(beacons: [AnyObject]!)
    func didReceiveRangingError(error : NSError)
}

class BeaconManager: NSObject, CLLocationManagerDelegate {
    let locationManager = CLLocationManager()
    internal var delegate : BeaconManagerDelegate?
    
    /// Assign the delegate and request the authorization
    override init() {
        super.init()

        locationManager.delegate = self
        
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedAlways) {
            locationManager.requestAlwaysAuthorization()
        }
        
        let region = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D"), identifier: "Estimotes")
        locationManager.startMonitoringForRegion(region)
    }
    
    /// Start the ranging for new beacons
    func startRanging() {
        for region in locationManager.monitoredRegions {
            locationManager.startRangingBeaconsInRegion(region as! CLBeaconRegion)
        }
    }
    
    /// Stop the ranging for new beacons
    func stopRanging() {
        for region in locationManager.monitoredRegions {
            locationManager.stopRangingBeaconsInRegion(region as! CLBeaconRegion)
        }
    }
    
    func locationManager(manager: CLLocationManager!, didRangeBeacons beacons: [AnyObject]!, inRegion region: CLBeaconRegion!) {
        self.delegate?.didRangeBeacons(beacons)
    }
    
    func locationManager(manager: CLLocationManager!, rangingBeaconsDidFailForRegion region: CLBeaconRegion!, withError error: NSError!) {
        self.delegate?.didReceiveRangingError(error)
    }
}
