//
//  Loader.swift
//  SmartLight
//
//  Created by Hans Knöchel on 26.01.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import UIKit
import MBProgressHUD

public class Loader: NSObject {
    var view : UIView
    var title : String
    
    /// Initialize the title and view
    init(view : UIView, title : String) {
        self.title = title
        self.view = view
        
        super.init()
    }
    
    /// Show the loader
    public func show() {
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.Indeterminate
        loadingNotification.labelText = self.title
    }
    
    /// Hide the loader
    public func hide() {
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
    }
}
