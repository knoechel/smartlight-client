//
//  BarNotification.swift
//  SmartLight
//
//  Created by Hans Knöchel on 05.02.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import UIKit
import CWStatusBarNotification

public class BarNotification: NSObject {
    var title : String
    
    /// Set the dialog title
    init(title : String) {
        self.title = title
    }
    
    /// Show the dialog
    public func show() {
        var notfication = CWStatusBarNotification()
        notfication.notificationStyle = .NavigationBarNotification
        notfication.notificationLabelBackgroundColor = UIColor.redColor()
        notfication.notificationLabelTextColor = UIColor.whiteColor()
        notfication.notificationAnimationInStyle = .Top
        notfication.notificationAnimationOutStyle = .Top
        notfication.displayNotificationWithMessage(self.title, forDuration: 2.0)
    }
}
