//
//  StringExtension.swift
//  SmartLight
//
//  Created by Hans Knöchel on 13.02.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import Foundation

extension String  {
    
    /// A c-based bridge to support md5-values in swift
    var md5: String! {
        let str = self.cStringUsingEncoding(NSUTF8StringEncoding)
        let strLen = CC_LONG(self.lengthOfBytesUsingEncoding(NSUTF8StringEncoding))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.alloc(digestLen)
        
        CC_MD5(str!, strLen, result)
        
        var hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }
        
        result.destroy()
        
        return String(format: hash as String)
    }
}