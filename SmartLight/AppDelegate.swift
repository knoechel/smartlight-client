//
//  AppDelegate.swift
//  SmartLight
//
//  Created by Hans Knöchel on 18.12.14.
//  Copyright (c) 2014 HS Osnabrück. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    /// The main appliction window
    var window: UIWindow?
    
    /// The beaconListener for backgroundServices
    let beaconListener = BeaconListener()

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        NSUserDefaults.standardUserDefaults().setValue("http://192.168.0.16:8080/api", forKey: "APIURL")
        
        self.toggleView()
        
        return true
    }
    
    /// Toggle the view when checking the user session
    func toggleView() {
        if NSUserDefaults.standardUserDefaults().boolForKey("loggedIn") {
            var mainStoryboard = UIStoryboard(name: "Home", bundle: nil)
            var home : HomeTabBarController = mainStoryboard.instantiateViewControllerWithIdentifier("HomeView") as! HomeTabBarController
            
            self.window?.rootViewController = home
        } else{
            var mainStoryboard = UIStoryboard(name: "Login", bundle: nil)
            var login : LoginTableViewController = mainStoryboard.instantiateViewControllerWithIdentifier("LoginView") as! LoginTableViewController
            
            self.window?.rootViewController = login
        }
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

