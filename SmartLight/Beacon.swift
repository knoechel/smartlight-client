//
//  Beacon.swift
//  SmartLight
//
//  Created by Hans Knöchel on 09.02.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import UIKit
import CoreLocation

public class Beacon: NSObject {
    private var beacon : CLBeacon
    public var selected : Bool = false
    
    init(beacon:CLBeacon) {
        self.beacon = beacon;
    }
    
    public func asDictionary() -> Dictionary<String, AnyObject> {
        return [
            "proximityUUID" : self.beacon.proximityUUID.UUIDString,
            "major": self.beacon.major,
            "minor": self.beacon.minor
        ]
    }
    
    public func getBeacon() -> CLBeacon {
        return self.beacon;
    }
    
    public func setBeacon(beacon : CLBeacon) -> Void {
        self.beacon = beacon
    }
}