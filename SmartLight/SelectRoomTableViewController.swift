//
//  SelectRoomTableViewController.swift
//  SmartLight
//
//  Created by Hans Knöchel on 05.02.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SelectRoomTableViewController: UITableViewController {

    /// Store the selected lights
    var selectedLights = [Int]()

    /// Store the selected beacons
    var selectedBeacons = [Beacon]()
    
    /// The room text field
    @IBOutlet weak var roomTextField: UITextField!

    /// The room save button
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
   @IBAction func cancelAction(sender: UIBarButtonItem) {
        self.closeWindow()
    }
    
    /// The segue used when the lights are selected
    ///
    /// :param: segue The corresponding segue of the transition
    @IBAction func unwindLightSelectorToList(segue: UIStoryboardSegue) {
        let source : SelectLightTableViewController = segue.sourceViewController as! SelectLightTableViewController
        self.selectedLights = source.selectedLights
        
        self.toggleSaveButton()
    }
    
    /// The segue used when the beacons are selected
    ///
    /// :param: segue The corresponding segue of the transition
    @IBAction func unwindBeaconSelectorToList(segue: UIStoryboardSegue) {
        let source : SelectBeaconTableViewController = segue.sourceViewController as! SelectBeaconTableViewController
        self.selectedBeacons = source.selectedBeacons
        
        self.toggleSaveButton()
    }

    /// Save the new room
    ///
    /// :param: sender The sender of the event
    @IBAction func saveAction(sender: UIBarButtonItem) {
        let url : String = NSUserDefaults.standardUserDefaults().stringForKey("APIURL")!
        
        let loader = Loader(view: self.navigationController!.view, title: "Adding room ...")
        loader.show()
        
        var beacons: [Dictionary<String, AnyObject>] = []
        
        for beacon in self.selectedBeacons {
            beacons.append(beacon.asDictionary())
        }
        
        let request = Alamofire.request(.POST, url+"/rooms", parameters: ["name": self.roomTextField.text, "lights": self.selectedLights, "beacons": beacons], encoding: .JSON)
            .responseJSON { (req, res, json, error) in
                
                loader.hide()
                
                if(json == nil) {
                    BarNotification(title: "The server is currently not available!").show()
                    return;
                }
                
                let responseData = JSON(json!)
                
                if(res?.statusCode != 200) {
                    Utilities().showAlert("Network problem", message: responseData["message"].stringValue, view: self)
                } else {
                    self.closeWindow()
                }
        }
    }
  
    /// Trigger the save button when the text field value is changed
    ///
    /// :param: sender The sender of the event
    @IBAction func setRoomValue(sender: UITextField) {
        self.toggleSaveButton()
    }
    
    /// Enable / disable the save button
    func toggleSaveButton() {
        self.saveButton.enabled = count(self.roomTextField.text) > 0 && self.selectedLights.count > 0 && self.selectedBeacons.count > 0
    }
    
    /// Close the selector window
    func closeWindow() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
}
