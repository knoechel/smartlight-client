//
//  Light.swift
//  SmartLight
//
//  Created by Hans Knöchel on 09.02.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import UIKit

public class Light: NSObject {
    private var id : Int!
    private var name : String!
    public var selected : Bool = false
    public var isOnline : Bool = false
    
    override init() {
        super.init()
    }
    
    public func getId() -> Int {
        return self.id
    }
    
    public func getName() -> String {
        return self.name;
    }
    
    public func setName(name : String) -> Void {
        self.name = name
    }
    
    public func setId(id : Int) -> Void {
        self.id = id
    }
}
