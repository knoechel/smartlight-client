//
//  BeaconListener.swift
//  SmartLight
//
//  Created by Hans Knöchel on 11.02.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON

class BeaconListener: NSObject, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    
    /// Assign the delegate and request the authorization
    override init() {
        super.init()
        
        locationManager.delegate = self;
        
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedWhenInUse) {
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    /// Toggle the light state in the background
    func toggleLightState(state : Int) {
        let url : String = NSUserDefaults.standardUserDefaults().stringForKey("APIURL")!
        let request = Alamofire.request(.POST, url+"/rooms/54f4cbc4166d230d0fd4464e/state", parameters: ["lightState": state],encoding: .JSON)
            .responseJSON { (req, res, json, error) in
                if(json == nil) {
                    return;
                }
                
                let responseData = JSON(json!)
                println(responseData)
        }
    }
    
    /// Register a beacon region
    func registerRegion() {
        let region = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D"), identifier: "Estimotes")
        locationManager.startMonitoringForRegion(region)
    }
    
    func locationManager(manager: CLLocationManager!, didRangeBeacons beacons: [AnyObject]!, inRegion region: CLBeaconRegion!) {
        NSLog("Beacon in range");
    }
    
    func locationManager(manager: CLLocationManager!, rangingBeaconsDidFailForRegion region: CLBeaconRegion!, withError error: NSError!) {
        println(error)
    }
    
    func locationManager(manager: CLLocationManager!, didEnterRegion region: CLRegion!) {
        manager.startRangingBeaconsInRegion(region as! CLBeaconRegion)
        manager.startUpdatingLocation()
        
        NSLog("You entered the region")
        self.toggleLightState(1)
    }
    
    func locationManager(manager: CLLocationManager!, didExitRegion region: CLRegion!) {
        manager.stopRangingBeaconsInRegion(region as! CLBeaconRegion)
        manager.stopUpdatingLocation()
        
        NSLog("You exited the region")
        self.toggleLightState(0)
    }
}
