//
//  SelectLightTableViewController.swift
//  SmartLight
//
//  Created by Hans Knöchel on 27.01.15.
//  Copyright (c) 2015 HS Osnabrück. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SelectLightTableViewController: UITableViewController {

    /// Store the raw lights json-response
    private var lights : JSON = nil

    /// Store all lights models
    private var items : NSMutableArray = []

    /// Store the selected light-ids
    internal var selectedLights = [Int]()

    /// The submit button
    @IBOutlet weak var submitButton: UIBarButtonItem!
    
    /// Force refresh
    ///
    /// :param: sender The sender of the event
    @IBAction func pulledRefreshAction(sender: UIRefreshControl) {
        self.searchLights()
    }
    
    /// Cancel the selection and close the current window
    ///
    /// :param: sender The sender of the event
    @IBAction func cancelAction(sender: UIBarButtonItem) {
        self.closeWindow()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchLights()
    }

    /// Toggle the submit button
    func toggleSubmitButton() {
        self.submitButton.enabled = (self.selectedLights.count > 0)
    }
    
    /// Close the current window
    func closeWindow() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    /// Display all lights
    func displayLights() -> Void {
        self.items.removeAllObjects()
        
        for (key: String, subJson: JSON) in self.lights {
            let index : Int = key.toInt()!
            
            let id = self.lights[index]["id"].intValue
            let name = self.lights[index]["name"].stringValue
            let isOnline = self.lights[index]["state"]["reachable"].boolValue
            
            var light = Light()
            light.setId(id)
            light.setName(name)
            light.isOnline = isOnline
            
            self.items.addObject(light)
        }
        
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
   /// Search for new lights
   func searchLights() -> Void {
        let url : String = NSUserDefaults.standardUserDefaults().stringForKey("APIURL")!
        
        let loader = Loader(view: self.navigationController!.view, title: "Searching lights ...")
        loader.show()
        
        let request = Alamofire.request(.GET, url+"/hue/lights", encoding: .JSON)
            .responseJSON { (req, res, json, error) in
                
                loader.hide()
                
                if(json == nil) {
                    self.refreshControl?.endRefreshing()
                    BarNotification(title: "The server is currently not available!").show()
                    return;
                }
                
                let responseData = JSON(json!)
                
                if(res?.statusCode != 200) {
                    BarNotification(title:responseData["message"].stringValue)
                } else {
                    self.lights = responseData
                    self.displayLights()
                }
        }
        println(request)
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("LightCell", forIndexPath: indexPath) as! UITableViewCell
        let myItem = self.items.objectAtIndex(indexPath.row) as! Light
        
        var lightStatus = cell.viewWithTag(100)
        var lightName = cell.viewWithTag(101) as! UILabel
       
        lightStatus?.backgroundColor = (myItem.isOnline) ? UIColor(rgba: "#17810f") : UIColor(rgba: "#ccc")
        lightName.text = myItem.getName()
        cell.accessoryType = (myItem.selected) ? .Checkmark : .None
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) -> Void {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let tappedItem = self.items.objectAtIndex(indexPath.row) as! Light
        tappedItem.selected = !tappedItem.selected
        
        if(contains(self.selectedLights, tappedItem.getId())) {
            self.selectedLights.removeAtIndex(find(self.selectedLights, tappedItem.getId())!)
        } else {
            self.selectedLights.append(tappedItem.getId())
        }
        
        self.toggleSubmitButton()
        
        tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
    }

}
